# URDF parsing an kinematics 
from urdf_parser_py.urdf import URDF
from pykdl_utils.kdl_kinematics import KDLKinematics
import numpy as np

base_link = "panda_link0"
end_link =  "panda_link8"
robot_urdf = URDF.from_xml_file('panda.urdf') 
kdl_kin = KDLKinematics(robot_urdf , base_link, end_link)

# set robot configuration
q = np.array([0.0, 0.0, 0.0, -1.5708, 0.0, 1.8675, 0.0])

# calculate the forward kinemtics
T = kdl_kin.forward(q)

# calculate inverse kinematics
q_ik = kdl_kin.inverse(T)


print("Robot configuration \nq_o={}".format(q))
print("Robot end-efector pose \nT={}".format(T))
print("IK solution for robot configuration \nq_ik={}".format(q_ik))
