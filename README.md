# Python KDL examples (PyKDL)

![](https://www.orocos.org/files/logo-t.png)


# Downloading the repository
## Using terminal
> make sure to clone the submodules - using `--recursive` option
```
git clone --recursive https://gitlab.inria.fr/auctus-team/people/antunskuric/example/pykdl_examples.git
```
## By zip download
If you prefer to download the zip file instead of using the terminal, two steps are needed:
1) Download and unzip the examples folder: https://gitlab.inria.fr/auctus-team/people/antunskuric/example/pykdl_examples
2) download and unzip the hrk-kdl repo: https://gitlab.inria.fr/auctus-team/people/antunskuric/ros/ros_nodes/hrl-kdl

Your folder structure should be like this:
```
pykdl_examples
    ├── hrl-kdl
    |   |
    |   ├── hrl_geom
    |   ├── ...
    |   └── pykdl_utils
    |
    ├──  env.yaml
    ├──  ...
    └──  panda_force_polytope.py

```

# Setting up the environment
## Using yaml file
The simplest way to setup the environment is using the `env.yaml` file. It will create a minimal environment called `pykdl_examples`, which you can use as a starting point for your developement.
```bash
conda env create -f env.yaml
conda activate pykdl_examples
```
## Create a custom environment
You can also simply use anaconda to create a new custom environment:
```bash
conda create -n pykdl_examples python=3 # create python 3 based environment
conda activate pykdl_examples           # activate the environment
```
Then you install conda requirements
```bash 
conda install -c conda-forge python-orocos-kdl     
conda install -c conda-forge catkin_pkg
conda install matplotlib 
```
Afterwards the global pip requirements
```bash
pip install urdf_parser_py
pip install pycapacity
```
Finally we install the local pip packages `hrl-kdl`
```bash
pip install -e ./hrl-kdl/hrl_geom
pip install -e ./hrl-kdl/pykdl_utils
```
## Running the example
Once your environment is set you should be able to run
```bash
python panda_force_polytope.py
```