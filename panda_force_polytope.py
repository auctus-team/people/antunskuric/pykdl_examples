# URDF parsing an kinematics 
from urdf_parser_py.urdf import URDF
from pykdl_utils.kdl_kinematics import KDLKinematics
import numpy as np

base_link = "panda_link0"
end_link =  "panda_link8"
robot_urdf = URDF.from_xml_file('panda.urdf') 
kdl_kin = KDLKinematics(robot_urdf , base_link, end_link)

t_max = np.array([kdl_kin.joint_limits_effort]).T 
t_min = -t_max

# polytope python module
import pycapacity.robot as capacity_solver

q = np.array([0.00138894,5.98736e-05,-0.00259058,   -1.69992, -6.64181e-05,    1.56995,-5.1812e-05])

Jac = np.array(kdl_kin.jacobian(q)[:3, :])
gravity = np.array([kdl_kin.gravity(q)]).T

print(Jac)

vertices, faces_index =  capacity_solver.force_polytope_withfaces(Jac, t_max, t_min, gravity)
faces = capacity_solver.face_index_to_vertex(vertices,faces_index)

print(np.round(kdl_kin.jacobian(q),3))

# plotting the polytope
import matplotlib.pyplot as plt
from pycapacity.visual import plot_polytope_faces, plot_polytope_vertex # pycapacity visualisation tools
fig = plt.figure()
# draw faces and vertices
ax = plot_polytope_vertex(plt=plt, vertex=vertices, label='force polytope',color='blue')
plot_polytope_faces(ax=ax, faces=faces, face_color='blue', edge_color='blue', alpha=0.2)

plt.tight_layout()
plt.legend()
plt.show()

